import * as React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { TextInput,Button ,ActivityIndicator } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import { Image  } from 'react-native';
import { CreditCardInput } from "react-native-credit-card-input";

import {FormBuilder} from 'react-native-paper-form-builder';
import {useForm} from 'react-hook-form';
import axios from "axios";
import { Text as Content }  from 'react-native-paper';





export default function Login({ navigation }) {
 const [email, setEmail] = React.useState("");
 const [contrasena, setContrasena] = React.useState("");
const baseURL = "http:/192.168.50.16:5200";

const {control, setFocus, handleSubmit} = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    mode: 'onChange',
  });




React.useEffect(() => {



navigation.setOptions({ title: `` });
     
	

    return () => {
      
    };
  }, []);


function pagar(){


}


async function cargar(nombre,email,documento) {
    try {
        await EncryptedStorage.setItem(
            "nombre",
           nombre
        );
await EncryptedStorage.setItem(
            "email",
           email
        );
await EncryptedStorage.setItem(
            "documento",
           documento
        );



        // Congrats! You've just stored your first value!
    } catch (error) {
        // There was an error on the native side
    }
}

  return (



 <View           style={styles.login}   >


<Image 

       style={styles.imagen}


        
        source={require('./logo.png')}
      />

     

       <Content variant="titleLarge" style={styles.logo}>Ingrese a su cuenta</Content>






<FormBuilder
          control={control}
          setFocus={setFocus}


          formConfigArray={[
            {
              type: 'email',
              name: 'email',

              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
pattern: {
                value:
                  /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                message: 'El email no es valido',
              },
 maxLength: {
                value: 30,
                message: 'El email debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Email',
activeOutlineColor: "#87CEFA",
            style: styles.boton,

              },
            },
            {
              type: 'password',
              name: 'password',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 30,
                message: 'La contrasena debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Contrasena',
activeOutlineColor: "#87CEFA",
       style: styles.boton,
              },
            },
          ]}
        />


<Button textColor="#87CEFA"  mode="text" style={styles.crear} contentStyle={styles.buttoncontent}  >
    Recuperar contrasena
  </Button>
    



<Button buttonColor="#87CEFA" mode='contained'  style={styles.crear}    onPress={handleSubmit((data: any) => {
axios.get(`${baseURL}/usuarios/${data.email}`).then((response) => {
if(response.data[0].contrasena===data.password){

cargar(response.data[0].nombre,data.email,response.data[0].documento);


navigation.navigate('Main');

}

console.log(response.data);


      
    }).catch(error => {
      console.log(error);
    });


            console.log( data);
          })}   contentStyle={styles.buttoncontent} >
    Ingresar  
  </Button>

<Button buttonColor="#87CEFA" mode="contained" onPress={() => navigation.navigate('Signin')} style={styles.crear} contentStyle={styles.buttoncontent} >
    No tienes cuenta? Registrate
  </Button>


<Button  mode="text" textColor="#87CEFA" style={styles.crear} contentStyle={styles.buttoncontent}  >
    Contactanos
  </Button>













</View>
  );
}

const styles = StyleSheet.create({
   button: {
    backgroundColor: "white",
margin: 20,
marginHorizontal: 10,
width: "80%",
height: 150,

  },
 modal: {

margin: 50,

height: 50,








  },

crear: {

marginHorizontal: "10%",



height: "10%",
width: "80%",
marginTop: 20,








  },
boton: {

marginHorizontal: "2%",

 

  },

content: {

marginTop: 100,

 

  },
buttoncontent: {

width: "100%",
height: "100%",

  },
logo: {
marginTop: 20,
marginLeft: 20,

width: "100%",
height: "10%",

  },
imagen: {
marginLeft: 20,
marginTop: 20,
width: "60%",
    height: "10%",
    resizeMode: 'stretch',

  },

icon: {




  },

login: {


backgroundColor: "white",
height: "100%",

  },
content: {
    
    flexDirection: "row",
  },
//...
});

