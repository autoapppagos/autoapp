import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet } from 'react-native';
import { Provider as PaperProvider  } from 'react-native-paper';
import { createStackNavigator } from '@react-navigation/stack';
import Signin from './Signin.js';
import Login from './Login.js';
import Main from './Main.js';
import Carros from './Carros.js';
import Placa from './Placa.js';
import Multas from './Multas.js';




import Credito from './Credito.js';
import Documentos from './Documentos.js';
import Retiros from './Retiros.js';


const Stack = createStackNavigator();

export default function App() {
  return (
<PaperProvider>
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Signin" component={Signin} />
        <Stack.Screen name="Login"  component={Login} />
<Stack.Screen name="Main"  component={Main} />
<Stack.Screen name="Carros"  component={Carros} />
<Stack.Screen name="Placa"  component={Placa} />
<Stack.Screen name="Multas"  component={Multas} />
<Stack.Screen name="Credito"  component={Credito} />
<Stack.Screen name="Documentos"  component={Documentos} />
<Stack.Screen name="Retiros"  component={Retiros} />
      </Stack.Navigator>
    </NavigationContainer>
</PaperProvider>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});


