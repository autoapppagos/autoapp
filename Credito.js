import * as React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { TextInput,Button ,ActivityIndicator } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import { Image  } from 'react-native';
import { CreditCardInput } from "react-native-credit-card-input";
import { WebView } from 'react-native-webview';
import { SafeAreaView } from "react-native";
import axios from "axios";
import {NetworkInfo} from "react-native-network-info";

import Notifications from './Notifications';










export default function Credito({ navigation }) {

 const [nombre, setNombre] = React.useState("");

const [indicator, setIndicator] = React.useState(0);

const [id, setId] = React.useState("");

const [ip, setIp] = React.useState("");


const baseURL = "http://192.168.50.16:5200";

const customHTML = `

<head>
  
    
<meta name="viewport" content="width=device-width,               maximum-scale=1, user-scalable=0">


   
  </head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=${id}"></script>
<script type="text/javascript">
  var wpwlOptions = {
    locale: 'es',
    style: 'card',
    maskCvv: true,
    labels: {
      cvv: 'CVV',
     
      expiryDate: 'Expira',
      brand: 'Tipo de Tarjeta',
    },
    onBeforeSubmitCard: function () {
      if ($('.wpwl-control-cardHolder').val() === '') {
        $('.wpwl-control-cardHolder').addClass('wpwl-has-error');
        $('.wpwl-control-cardHolder').after(
          "<div class='wpwl-hint-cardHolderError' style='color:red;'>Campo requerido</div>",
        );
        $('.wpwl-button-pay')
          .addClass('wpwl-button-error')
          .attr('disabled', 'disabled');
        return false;
      } else return true;
    },
  };
</script>

<body>
  
  <div>
    <form
      action="https://blog.logrocket.com/react-native-webview-a-complete-guide/"
      class="paymentWidgets"
      data-brands="VISA MASTER AMEX"></form>
  </div>
</body>

<style>
  
  .paymentWidgets {
    background-color: pink;
  }
  input {
    background-color: pink;
  }
  body {
    font-family: Verdana;
    background-color: #eaeff5;
  }
  .wpwl-form {
    background-color: white;
    border-radius: 1rem;
    font-family: Verdana;
    padding: 1rem;
    margin: 0.5rem;
  }
  .wpwl-label {
    font-family: Verdana;
    margin-bottom: 0.2rem;
    margin-left: 0.1rem;
  }
  .wpwl-control {
    background-color: #eaeff5;
    border-radius: 0.5rem;
    padding: 0.2rem 0 0.2rem 0.5rem
  }
  /* .wpwl-control:focus {
    background-color: white;
  } */
  .wpwl-label-brand {
    width: 50%;
  }
  .wpwl-wrapper-brand {
    padding: 0;
    width: 50%;
  }
  .wpwl-brand-card {
    padding: 0;
    margin: 0;
  }
  .wpwl-button {
    background: linear-gradient(
      36deg,
      rgba(84, 62, 248, 1) 0%,
      rgba(153, 154, 213, 1) 72%,
      rgba(115, 131, 255, 1) 100%
    );
    border: none;
    border-radius: 0.5rem;
    width: 50%;
    margin-top: 0.5rem;
  }
</style>
`;


async function cargar() {
    try {   
        const credito = await EncryptedStorage.getItem("credito");
        const email = await EncryptedStorage.getItem("email");



axios.get(`${baseURL}/usuarios/${email}`).then((response) => {

console.log(response.data[0].nombre);


const params = new URLSearchParams({
          'entityId': '8ac7a4ca8810780f01882096776e090b',
    'amount': credito,
    'currency': 'USD',
    'paymentType': 'DB',
'customer.givenName': response.data[0].nombre,
'customer.middleName': response.data[0].nombre2,
'customer.surname': response.data[0].apellido,
'customer.ip': ip,
'customer.merchantCustomerId': response.data[0].usuarioid,
'merchantTransactionId' : `${response.data[0].usuarioid}_${Date.now()}`,
'customer.email': response.data[0].email,
'customer.identificationDocType': 'IDCARD',
'customer.identificationDocId': response.data[0].documento,
'customer.phone': response.data[0].telefono,
'shipping.street1': response.data[0].adress,
'billing.street1': response.data[0].adress,
'shipping.country': 'EC',
'billing.country': 'EC',
'testMode' 	: 'EXTERNAL',
'cart.items[0].name': 'Multa',
'cart.items[0].description' 	: 'Pago de multa',
'cart.items[0].price' 	: '30.00',
'cart.items[0].quantity' 	: '1',
'customParameters[SHOPPER_VAL_BASE0]': '30.00',
'customParameters[SHOPPER_VAL_BASEIMP]': '0.00',
'customParameters[SHOPPER_VAL_IVA]': '0.00',
'customParameters[SHOPPER_MID]': '1000000505',
'customParameters[SHOPPER_TID]': 'PD100406',
'customParameters[SHOPPER_ECI]': '0103910',
'customParameters[SHOPPER_PSERV]': '17913101',
'risk.parameters[USER_DATA2]': 'JR Group',
        }).toString();


axios.post(`https://test.oppwa.com/v1/checkouts?${params}`,null,{headers: { Authorization :  `Bearer OGE4Mjk0MTg1YTY1YmY1ZTAxNWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA==


` }}).then((response) => {

setId(response.data.id);

console.log(response.data.id);
      
    }).catch(error => {
      console.log(error);
    });


      
    }).catch(error => {
      console.log(error);
    });








  



    } catch (error) {
        // There was an error on the native side
    }
}

async function guardar() {  
   

axios.get(`https://test.oppwa.com/v1/checkouts/${id}/payment?entityId=8ac7a4ca8810780f01882096776e090b`,{headers: { Authorization :  `Bearer OGE4Mjk0MTg1YTY1YmY1ZTAxNWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA==


` }}).then((response) => {



console.log(response.data);
      
    }).catch(error => {
      console.log(error);
    });
  
}



React.useEffect(() => {



NetworkInfo.getIPAddress().then(ip => {
  
  setIp(ip);
});

cargar();

navigation.setOptions({ title: `Pagar multa` ,headerRight: () => (
           <Notifications navigation={navigation} />
          )});
     
	

    return () => {
      
    };
  }, []);

function pagar(){

setIndicator(1);

setTimeout(() => {

setIndicator(0);
alert("La multa ha sido pagada");

},
    5000
);

}
 


  return (

 

<SafeAreaView style={{ flex : 1}}>
        <WebView 
          source={{ html: customHTML }} 
onNavigationStateChange={(navState) => {

if(navState.title==="about:blank"){


}
else{

console.log(navState);
guardar();

navigation.navigate('Main');
alert("Se ha realizado el pago");
}


}}
        />
      </SafeAreaView>


  );
}

const styles = StyleSheet.create({
   button: {
    backgroundColor: "white",
margin: 20,
marginHorizontal: 10,
width: "80%",
height: 150,

  },
 modal: {

margin: 50,

height: 50,








  },
buttoncontent: {

width: "100%",
height: "100%",

  },

icon: {




  },
content: {
    
    flexDirection: "row",
  },
//...
});