import * as React from 'react';
import {StyleSheet,ScrollView,View} from 'react-native';
import { TextInput,Button,Modal, Portal,Text,DataTable,ActivityIndicator  } from 'react-native-paper';
import DropDown from "react-native-paper-dropdown";
import axios from 'axios';
import XMLParser from 'react-xml-parser';
import EncryptedStorage from 'react-native-encrypted-storage';
import {FormBuilder} from 'react-native-paper-form-builder';
import {useForm} from 'react-hook-form';



import Notifications from './Notifications';

export default function Placa({ navigation }) {
 const [placa, setPlaca] = React.useState("");
 const [tipo, setTipo] = React.useState("");
 const [documento, setDocumento] = React.useState("");
const [propietario, setPropietario] = React.useState("");
const [marca, setMarca] = React.useState("");
const [modelo, setModelo] = React.useState("");
const [color, setColor] = React.useState("");
const [clase, setClase] = React.useState("");
const [servicio, setServicio] = React.useState("");
const [fecha, setFecha] = React.useState("");
const [email, setEmail] = React.useState("");

const [placatipo, setPlacatipo] = React.useState(0);
const [documentotipo, setDocumentotipo] = React.useState(0);
const [indicator, setIndicator] = React.useState(0);


const documentos = [
    {
      label: "Cedula",
      value: "CED",
    },
    {
      label: "Pasaporte",
      value: "PAS",
    },
    {
      label: "RUC",
      value: "RUC",
    }
  ];

const {control, setFocus, handleSubmit} = useForm({
    defaultValues: {
      placa: '',
      tipo: '',
documento: '',
    },
    mode: 'onChange',
  });

const baseURL = "http://192.168.50.9:5200";

function cargarcarro(placa,documento,tipo){

console.log(placa.replace('-','').toUpperCase());

axios.get(`${baseURL}/placa/${placa.replace('-','').toUpperCase() }`).then((response) => {
      console.log(response.data);
const xml = new XMLParser().parseFromString(response.data); 


if(typeof(xml.getElementsByTagName('tipoIdent')[0])!=="undefined" && xml.getElementsByTagName('docPropietario')[0].value===documento && xml.getElementsByTagName('tipoIdent')[0].value===tipo ){

setPropietario(xml.getElementsByTagName('nombreBenef')[0].value);
setMarca(xml.getElementsByTagName('marca')[0].value);
setColor(xml.getElementsByTagName('colorDesc')[0].value);
setClase(xml.getElementsByTagName('claseVehiculoDesc')[0].value);
setModelo(xml.getElementsByTagName('modelo')[0].value);
setServicio(xml.getElementsByTagName('tipoServicioDesc')[0].value);
setFecha(xml.getElementsByTagName('anio')[0].value);
setPlaca(xml.getElementsByTagName('placaActual')[0].value);

setPlacatipo(1);







}
else{

alert("No existe el carro");

}
    }).catch(error => {
      console.log(error);
    });



}


 function guardar(){




axios.get(`${baseURL}/usuarios/${email}`)
	      .then((response) => {
axios.post(`${baseURL}/carros`, {
	        propietario: propietario,
	        marca: marca,
	        modelo: modelo,
	        color: color,
 clase: clase,
 servicio: servicio,
 fecha: fecha,
usuarioid: response.data[0].usuarioid,
email: response.data[0].email,
placa: placa.replace('-','').toUpperCase(),
 





	
	      })
	      .then((response) => {



navigation.navigate('Main')




	        
	      }).catch(error => {
	      console.log(error);
	    });



 }).catch(error => {
	      console.log(error);
	    });

}




async function cargar() {
    try {   
        const session = await EncryptedStorage.getItem("email");




    
      setEmail(session);

    } catch (error) {
        // There was an error on the native side
    }
}

React.useEffect(() => {

cargar();

navigation.setOptions({ title: `Pagar multa` ,headerRight: () => (
           <Notifications navigation={navigation} />
          )});




     
	

    return () => {
      
    };
  }, []);
React.useEffect(() => {






     
	

    return () => {
      
    };
  }, [indicator]);



  return (

 <View                                               style={styles.logo}       >

    <FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[
            {
              type: 'text',
              name: 'placa',

              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },

 maxLength: {
                value: 30,
                message: 'La placa debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Placa',
activeOutlineColor: "#87CEFA",
            style: styles.boton,

              },
            },
{
            name: 'tipo',
            type: 'select',
            textInputProps: {
              label: 'Tipo de documento',
activeOutlineColor: "#87CEFA",
            style: styles.boton,
              
            },
            rules: {
              required: {
                value: 1,
                message: 'Debe seleccionar el tipo de documento',
              },
            },
            options: [
              {
                label: 'Cedula',
                value: 'CED',
              },
              {
                label: 'RUC',
                value:'RUC',
              },
              {
                label: 'Pasaporte',
                value: 'PAS',
              },
             
            ],
          },
            {
              type: 'text',
              name: 'documento',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 30,
                message: 'El numero de documento debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Numero de documento',
activeOutlineColor: "#87CEFA",
            style: styles.boton,
              },
            },
          ]}
        />
   

<Button buttonColor="#87CEFA"  mode="contained" onPress={handleSubmit((data: any) => {
            console.log( data);
setIndicator(1);
console.log(indicator);
cargarcarro(data.placa,data.documento.replace('-','').toUpperCase(),data.tipo);



          })} style={styles.outlined} contentStyle={styles.buttoncontent} >
    Registrar carro
  </Button>

  <ActivityIndicator animating={indicator} />


 <Portal>
        <Modal  visible={placatipo} theme={{colors: {backdrop: 'rgba(255, 255, 255, 255)'}}} onDismiss={() => setPlacatipo(0)}  >

<View>
          <DataTable>
      <DataTable.Row>
        <DataTable.Title>Propietario</DataTable.Title>


<DataTable.Cell>{propietario }</DataTable.Cell>



        
      </DataTable.Row>

<DataTable.Row>
        
<DataTable.Title>Marca</DataTable.Title>


<DataTable.Cell>{marca }</DataTable.Cell>


        
      </DataTable.Row>

      <DataTable.Row>
        




<DataTable.Title>Modelo</DataTable.Title>

<DataTable.Cell>{modelo }</DataTable.Cell>

        
      </DataTable.Row>

<DataTable.Row>
        




<DataTable.Title>Color</DataTable.Title>

<DataTable.Cell>{color }</DataTable.Cell>

        
      </DataTable.Row>


<DataTable.Row>
        

<DataTable.Title>Clase</DataTable.Title>

<DataTable.Cell>{clase }</DataTable.Cell>

        
      </DataTable.Row>

<DataTable.Row>
        




<DataTable.Title>Servicio</DataTable.Title>


<DataTable.Cell>{servicio }</DataTable.Cell>
        
      </DataTable.Row>

<DataTable.Row>
        






<DataTable.Title>Fecha</DataTable.Title>

<DataTable.Cell>{fecha }</DataTable.Cell>
        
      </DataTable.Row>


     

      
    </DataTable>
<Button  mode="contained" onPress={guardar} >
    Guardar carro
  </Button>
</View>
        </Modal>
      </Portal>

</View>
  );
}



const styles = StyleSheet.create({
  boton: {

marginHorizontal: "2%",

 

  },

outlined: {

margin: 50,

height: 50,








  },
buttoncontent: {

width: "100%",
height: "100%",

  },
logo: {

 backgroundColor: "white",


  },



//...
});

