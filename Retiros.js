import * as React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { TextInput,Button,Menu  } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import { Image  } from 'react-native';
import axios from "axios";
import FormData from 'form-data';
import { RNCamera } from 'react-native-camera';
import RNFS from 'react-native-fs';
import { ALERT_TYPE, Dialog, AlertNotificationRoot, Toast } from 'react-native-alert-notification';
import { Surface, Text as Content } from 'react-native-paper';
import Notifications from './Notifications';



export default function Retiros ({ navigation }) {

 

const [cedula, setCedula] = React.useState("");
const [token, setToken] = React.useState("");
const [imagen, setImagen] = React.useState("");
const [email, setEmail] = React.useState("");
const [disabled, setDisabled] = React.useState(0);
const [indicator, setIndicator] = React.useState(0);
const [button, setButton] = React.useState("Grabar");
const cameraRef = React.useRef();
const baseURL = "http://192.168.50.9:5200";







async function cargar() {
    try {   
        const res = await EncryptedStorage.getItem("email");
const documento = await EncryptedStorage.getItem("documento");


setCedula(documento);
setEmail(res);
console.log(documento);
console.log(res);







   



    } catch (error) {
        // There was an error on the native side
    }
}


React.useEffect(() => {

cargar();


navigation.setOptions({ title: `Pagar multa` ,headerRight: () => (
           <Notifications navigation={navigation} />
          )});






 return () => {
      
    };
  }, []);







async function grabar1(base64image1) {
    try {   

const data = await cameraRef.current.takePictureAsync({ quality: 0.5});

const base64image2 = await RNFS.readFile(data.uri, 'base64');

setButton("Espere un momento"); 

axios.post("https://sodignature.com/ApiSodig/api/Usuario/IniciarSesion", {
      Identificacion: cedula,
ip: "192.168.50.9",
correo: email,
aceptaTerminos: 1
    })
    .then((responsetoken) => {



      console.log(responsetoken.data);
axios.get(`https://sodignature.com/ApiSodig/api/Informacion/
ObtenerImagen?identificacion=${cedula}`,{
    headers: { Authorization: `Bearer ${responsetoken.data.obj.access_token}` }
}).then((response) => {

console.log(response.data);







axios.post("http://192.168.50.9:5201/usuarios", {
      imagen: base64image1,
imagen2: base64image2,
imagen1: response.data.obj[0].Valor,
token: responsetoken.data.obj.access_token,
cedula: cedula,
email: email
    })
    .then((response) => {
      console.log(response.data);
setButton("Grabar");
setIndicator(0);




Dialog.show({
          type: ALERT_TYPE.WARNING,
          title: 'Mensaje',
          textBody: response.data.mensajes,
          button: 'Cerrar',
        })

    })
.catch(error => {
      console.log(error);
alert("Ocurrio un error");
    });



}).catch(error => {
      console.log(error);
    });

    })
.catch(error => {
      console.log(error);
    });







} catch (error) {
console.log(error);
        // There was an error on the native side
    }


      

}



async function grabar() {
    try {   


setButton("Grabando");
setIndicator(1);


const data = await cameraRef.current.takePictureAsync({ quality: 0.5});


const base64image1 = await RNFS.readFile(data.uri, 'base64');

    

            







setTimeout(() => {

 grabar1(base64image1);

},
    2000
);

     







  
      

    } catch (error) {


   
    }
}




  return (
<AlertNotificationRoot>

 <View                style={styles.camera}       >




<RNCamera style={styles.rnCamera} type={RNCamera.Constants.Type.front} ref={cameraRef}

/>


<Button buttonColor="#87CEFA" mode='contained'  style={styles.modal} disabled={disabled} loading={indicator}   onPress={grabar}   contentStyle={styles.buttoncontent} >
    {button}  
  </Button>

 
     <Content         style={styles.surface}> Para la validación biométrica siga estas recomendaciones{'\n'}

 1.- No utilizar artículos como: Gafas, Mascarillas, lentes, gorras, etc.{'\n'}

 2.- Ubicarse en un sitio con buena iluminación, preferible luz natural sin reflejos como lámparas.{'\n'}

 3.- Evite realizar movimientos bruscos a la cámara mientras realice la prueba de vida.{'\n'}

  4.- Su mirada siempre debe estar dirigida a su cámara de video.</Content>
 





</View>
</AlertNotificationRoot>
  );
}

const styles = StyleSheet.create({
   surface: {
   
width: "100%",
height: 150,
marginTop: 20,
backgroundColor: "white",
  },

camera: {


height: "100%",
backgroundColor: "white",

  },
 modal: {
    
marginTop: 200,
marginHorizontal: 50,

height: 50,






  },
buttoncontent: {

width: "100%",
height: "100%",

  },

imagen: {
    width: 50,
    height: 50,
  },
rnCamera: {

  width: "100%",
    height: "30%",


   
  },
tinyLogo: {
    width: 50,
    height: 50,
  },



icon: {




  },



content: {
    
    flexDirection: "row",
  },
//...
});