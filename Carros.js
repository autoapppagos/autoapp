import * as React from 'react';
import {StyleSheet,ScrollView,View} from 'react-native';
import {DataTable,Banner, TextInput,Button  } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import axios from "axios";
import { Image  } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';

export default function Carros({actual}) {

const [carros, setCarros] = React.useState([]);

const [propietario, setPropietario] = React.useState("");
const [marca, setMarca] = React.useState("");
const [modelo, setModelo] = React.useState("");
const [color, setColor] = React.useState("");
const [clase, setClase] = React.useState("");
const [servicio, setServicio] = React.useState("");
const [fecha, setFecha] = React.useState("");


const baseURL = "http://192.168.50.16:5200";

async function cargar() {


console.log(`${baseURL}/carro/${actual}`);

axios.get(`${baseURL}/carro/${actual}`).then((response) => {

console.log(`${baseURL}/carro/${actual}`);

setPropietario(response.data[0].propietario);
setMarca(response.data[0].marca);
setColor(response.data[0].color);
setClase(response.data[0].clase);
setModelo(response.data[0].modelo);
setServicio(response.data[0].servicio);
setFecha(response.data[0].fecha);







      
    }).catch(error => {
      console.log(error);
    });
   
}



React.useEffect(() => {

cargar();

    return () => {
      
    };
  }, [actual]);

React.useEffect(() => {

cargar();

    return () => {
      
    };
  }, []);



return (

 <View >


<DataTable>
      <DataTable.Row>
        <DataTable.Title>Propietario</DataTable.Title>


<DataTable.Cell>{propietario }</DataTable.Cell>



        
      </DataTable.Row>

<DataTable.Row>
        
<DataTable.Title>Marca</DataTable.Title>


<DataTable.Cell>{marca }</DataTable.Cell>


        
      </DataTable.Row>

      <DataTable.Row>
        




<DataTable.Title>Modelo</DataTable.Title>

<DataTable.Cell>{modelo }</DataTable.Cell>

        
      </DataTable.Row>

<DataTable.Row>
        




<DataTable.Title>Color</DataTable.Title>

<DataTable.Cell>{color }</DataTable.Cell>

        
      </DataTable.Row>


<DataTable.Row>
        

<DataTable.Title>Clase</DataTable.Title>

<DataTable.Cell>{clase }</DataTable.Cell>

        
      </DataTable.Row>

<DataTable.Row>
        




<DataTable.Title>Servicio</DataTable.Title>


<DataTable.Cell>{servicio }</DataTable.Cell>
        
      </DataTable.Row>

<DataTable.Row>
        






<DataTable.Title>Fecha</DataTable.Title>

<DataTable.Cell>{fecha }</DataTable.Cell>
        
      </DataTable.Row>


     

      
    </DataTable>


    

</View>
  );

 


  
}

const styles = StyleSheet.create({
  surface: {
margin: 20,
    height: 80,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",

    
  },

buttoncontent: {



width: "100%",
height: "100%",

  },

indicator: {
marginTop: "40%",
   
    
 

    

    
  },
screen: {

    height: "60%",
    width: "100%",
    
 backgroundColor: "white",

    

    
  },
});