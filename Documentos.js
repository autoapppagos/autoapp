import * as React from 'react';
import {StyleSheet,ScrollView,View} from 'react-native';
import {DataTable,Banner, TextInput,Button,Text,Modal,Portal  } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import axios from "axios";
import { Image } from 'react-native';
import { Marker,MapView } from 'react-native-maps';
import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import { ActivityIndicator } from 'react-native-paper';



export default function Documentos({actual}) {

const [carros, setCarros] = React.useState([]);

const [banner, setBanner] = React.useState(0);

const [placatipo, setPlacatipo] = React.useState(0);



const [multas, setMultas] = React.useState([]);


const [carga, setCarga] = React.useState(0);

const [pago, setPago] = React.useState([]);

const [file, setFile] = React.useState([{ "uri": ""}]);

const [placa, setPlaca] = React.useState("");
const [email, setEmail] = React.useState("");


const [uri, setUri] = React.useState("");





const baseURL = "http://192.168.50.16:5200";

async function cargar() {
    try {   
        const email = await EncryptedStorage.getItem("email");

console.log(`${baseURL}/carros/${email}`);
setEmail(email);

}

catch (error) {
        // There was an error on the native side
    }


}

React.useEffect(() => {

cargar();

    return () => {
      
    };
  }, []);




function mostrar(placa){






setBanner(1);






}





async function handledocument(tipo) {
    try {   
await setCarga(1);

        const response = await DocumentPicker.pick({
       type: [DocumentPicker.types.allFiles],
presentationStyle: 'fullScreen',
      });
 

console.log(response);
const base64image = await RNFS.readFile(response[0].uri, 'base64');


axios.post(`${baseURL}/documentos`, {
      imagen: base64image,
email: email,
placa: actual,
tipo: tipo,

    })
    .then((response) => {


     
    })
.catch(error => {
      console.log(error);

    });

      
    
  
 await setCarga(0);

await alert("Se ha guardado la imagen");


    } catch (error) {
setCarga(0);
        // There was an error on the native side
    }
}


function informacion(tipo){

date=Date.now();

setUri(`${baseURL}/${email}-${actual}-${tipo}.jpeg?${date} `)







setPlacatipo(1);

console.log(uri);




   




}
 
 


  return (



 <View >

<Portal>
        <Modal  visible={carga} theme={{colors: {backdrop: 'rgba(255, 255, 255, 255)'}}}   >

<ActivityIndicator animating={1}  />

<View>




  

</View>
        </Modal>
      </Portal>




<ScrollView  >



<DataTable>

<DataTable.Row>
        




<DataTable.Title>Documento de identificacion</DataTable.Title>
<Button mode='contained-tonal'  style={styles.pago}    onPress={() => handledocument("identificacion")} icon={() => (
   <Image
      source={require('./upload.png')}
      style={styles.icon}
    />
  )}   contentStyle={styles.buttoncontent} >
     
  </Button>

<Button mode='contained-tonal'  style={styles.pago}    onPress={() => informacion("identificacion")} icon={() => (
   <Image
      source={require('./see.png')}
      style={styles.icon}
    />
  )}   contentStyle={styles.buttoncontent} >
    
  </Button>



        
      </DataTable.Row>
<DataTable.Row>
        








        
      </DataTable.Row>







     

      
    </DataTable>


<DataTable>

<DataTable.Row>
        




<DataTable.Title>Documento de vehiculo</DataTable.Title>
<Button mode='contained-tonal'  style={styles.pago}    onPress={() => handledocument("carro")} icon={() => (
   <Image
      source={require('./upload.png')}
      style={styles.icon}
    />
  )}   contentStyle={styles.buttoncontent} >
      
  </Button>
<Button mode='contained-tonal'  style={styles.pago}    onPress={() => informacion("carro")} icon={() => (
   <Image
      source={require('./see.png')}
      style={styles.icon}
    />
  )}    contentStyle={styles.buttoncontent} >
   
  </Button>



        
      </DataTable.Row>







     

      
    </DataTable>


 


 




 </ScrollView>


<Portal>
        <Modal  visible={placatipo}  onDismiss={() => setPlacatipo(0)} style={styles.tinyLogo} >

<View>


<Image 

       style={styles.imagen}
        
        source={{
          uri: uri,
        }}
      />

  

</View>
        </Modal>
      </Portal>













    





</View>
  );
}

const styles = StyleSheet.create({
  surface: {
margin: 20,
    height: 80,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",

    
  },
pago: {

    height: 80,
    width: 100,
marginHorizontal: 5,

backgroundColor: "white",
    

    
  },

buttoncontent: {



width: "100%",
height: "100%",

  },

multa: {

    backgroundColor: "white",

    
  },

tinyLogo: {
marginLeft: 50,
marginTop: 50,
    width: "80%",
height: "80%",
  },

imagen: {

    width: "100%",
height: "100%",
  },
});