import * as React from 'react';
import {StyleSheet,ScrollView,View} from 'react-native';
import {DataTable,Banner, TextInput,Button,Text,Modal,Portal  } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import axios from "axios";
import { Image  } from 'react-native';
import { Marker,MapView } from 'react-native-maps';




export default function Multas({navigation,actual,}) {

const [carros, setCarros] = React.useState([]);

const [banner, setBanner] = React.useState(0);

const [placatipo, setPlacatipo] = React.useState(0);



const [multas, setMultas] = React.useState([]);

const [multa, setMulta] = React.useState(0);




const [pago, setPago] = React.useState([]);



const baseURL = "http://192.168.50.16:5200";

async function cargar() {

axios.get(`${baseURL}/multas/${actual}`).then((response) => {

setMultas(response.data);
console.log(response.data.length);
setMulta(response.data.length);


setBanner(1);
console.log(multas);



      
    }).catch(error => {
      console.log(error);
    });
    
}

async function guardar() {
    try {   
        await EncryptedStorage.setItem(
            "credito",
           "30"
        );
navigation.navigate('Credito');
      
 


    } catch (error) {
        // There was an error on the native side
    }
}

React.useEffect(() => {

cargar();

    return () => {
      
    };
  }, [actual]);






function informacion(multa){



setPago(multa);



setPlacatipo(1);




   




}
 
 


  return (

 <View >

<Button textColor="#87CEFA"   mode="text"   >
    Tiene {multa} multas registradas
  </Button>



<ScrollView >

  {
                multas.map((multa) => { return <View                  style={styles.multa}        >

<DataTable>

<DataTable.Row>
        




<DataTable.Title>Fecha</DataTable.Title>


<DataTable.Cell>{multa.fecha }</DataTable.Cell>
        
      </DataTable.Row>

<DataTable.Row>
        




<DataTable.Title>Valor de multa</DataTable.Title>


<DataTable.Cell>{120 }</DataTable.Cell>
        
      </DataTable.Row>





     

      
    </DataTable>


   <ScrollView horizontal>

<Button mode='contained-tonal'  style={styles.pago}    onPress={() => informacion(multa) } icon={() => (
   <Image
      source={require('./multas.png')}
      style={styles.icon}
    />
  )}   contentStyle={styles.buttoncontent} >
   Informacion detallada
  </Button>
<Button mode='contained-tonal'  style={styles.pago}    onPress={() => {guardar();} } icon={() => (
   <Image
      source={require('./pagar.png')}
      style={styles.icon}
    />
  )}   contentStyle={styles.buttoncontent} >
   Pagar multa
  </Button>
</ScrollView>

</View>

 


})

            }

 </ScrollView>



<Portal>
        <Modal  visible={placatipo} style={styles.modal} onDismiss={() => setPlacatipo(0)}  >

<View>


<DataTable  style={styles.multa}>



        
     


<DataTable.Row>
        

<DataTable.Title>Codigo</DataTable.Title>

<DataTable.Cell>{pago.codigo }</DataTable.Cell>

        
      </DataTable.Row>

<DataTable.Row>
        




<DataTable.Title>Fecha</DataTable.Title>


<DataTable.Cell>{pago.fecha }</DataTable.Cell>
        
      </DataTable.Row>

<DataTable.Row>
        






<DataTable.Title>Direccion</DataTable.Title>

<DataTable.Cell>{pago.direccion }</DataTable.Cell>
        
      </DataTable.Row>

<DataTable.Row>
        






<DataTable.Title>Tipo</DataTable.Title>

<DataTable.Cell>{5}</DataTable.Cell>
        
      </DataTable.Row>

<DataTable.Row>
        






<DataTable.Title>Infraccion</DataTable.Title>

<DataTable.Cell>{"Deteccion por camara" }</DataTable.Cell>
        
      </DataTable.Row>




     

      
    </DataTable>

  

</View>
        </Modal>
      </Portal>










    





</View>
  );
}

const styles = StyleSheet.create({
  surface: {
margin: 20,
    height: 80,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "white",

    
  },
pago: {
margin: 20,
    height: 80,
    width: 200,
marginLeft: 1,
backgroundColor: "white",

    

    
  },
buttoncontent: {



width: "100%",
height: "100%",
flexDirection: 'column'

  },

modal: {
marginLeft: 50,
marginTop: 50,
    width: "80%",
height: "80%",
  },

icon: {
marginRight: 20,

  },




multa: {

    backgroundColor: "white",


    
  },
});