import * as React from 'react';
import {StyleSheet,ScrollView,View} from 'react-native';
import { TextInput,Button } from 'react-native-paper';
import DropDown from "react-native-paper-dropdown";
import axios from "axios";
import EncryptedStorage from 'react-native-encrypted-storage';
import {FormBuilder} from 'react-native-paper-form-builder';
import {useForm} from 'react-hook-form';
import XMLParser from 'react-xml-parser';
import { ActivityIndicator } from 'react-native-paper';

export default function Signin({ navigation }) {
 const [nombre, setNombre] = React.useState("");
 const [apellido, setApellido] = React.useState("");
 const [email, setEmail] = React.useState("");
 const [contrasena, setContrasena] = React.useState("");
 const [tipo, setTipo] = React.useState("");
 const [documento, setDocumento] = React.useState("");
 const [documentotipo, setDocumentotipo] = React.useState(0);

const documentos = [
    {
      label: "Cedula",
      value: "CED",
    },
    {
      label: "Pasaporte",
      value: "PAS",
    },
    {
      label: "RUC",
      value: "RUC",
    }
  ];

const [pago, setPago] = React.useState(0);

const {control, setFocus, handleSubmit} = useForm({
    defaultValues: {
      nombre: '',
nombre2: '',
      apellido: '',
email: '',
telefono: '',
direccion: '',
      contrasena: '',
documento: '',
    },
    mode: 'onChange',
  });

const baseURL = "http://192.168.50.16:5200";
const infractionsURL = "http://192.168.102.173:5200";


React.useEffect(() => {



navigation.setOptions({ title: `Crear usuario ` });
     
	

    return () => {
      
    };
  }, []);



async function cargar(nombre,email,documento) {
    try {
        await EncryptedStorage.setItem(
            "nombre",
           nombre
        );
await EncryptedStorage.setItem(
            "email",
           email
        );
await EncryptedStorage.setItem(
            "documento",
           documento
        );

navigation.navigate('Retiros');


        // Congrats! You've just stored your first value!
    } catch (error) {
        // There was an error on the native side
    }
}

function crearusuario(nombre,apellido,email,contrasena,documento,nombre2,telefono,adress){



axios.get(`${baseURL}/usuarios/${email}`).then((response) => {



if(typeof(response.data[0])==="undefined"){

axios.post(`${baseURL}/usuarios`, {
	        nombre: nombre,
	        apellido: apellido,
	        contrasena: contrasena,
	        email: email,
nombre2: nombre2,
telefono: telefono,
adress: adress
	
	      })
	      .then((response) => {




	        
	      }).catch(error => {
	      console.log(error);
	    });

  


	

}
else{


alert("El usuario esta registrado");







}

}).catch(error => {
      console.log(error);
    });

}

  return (
 <ScrollView >

<FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[

{
              type: 'text',
              name: 'nombre',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 30,
                message: 'El nombre debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Nombre',





activeOutlineColor: "#87CEFA",
            style: styles.boton,
              },
            },
{
              type: 'text',
              name: 'nombre2',

              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
 maxLength: {
                value: 30,
                message: 'El nombre debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Segundo nombre',

activeOutlineColor: "#87CEFA",
            style: styles.boton,







              },
            },
{
              type: 'text',
              name: 'apellido',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 30,
                message: 'El apellido debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Apellido',
             
activeOutlineColor: "#87CEFA",
            style: styles.boton,


 },
            },
            {
              type: 'email',
              name: 'email',

              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
pattern: {
                value:
                  /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                message: 'El email no es valido',
              },
 maxLength: {
                value: 30,
                message: 'El email debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Email',

activeOutlineColor: "#87CEFA",
            style: styles.boton,







              },
            },

  {
              type: 'text',
              name: 'telefono',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 10,
                message: 'El telefono debe tener un limite de 10 caracteres',
              },
              },
              textInputProps: {
                label: 'Telefono',

activeOutlineColor: "#87CEFA",
            style: styles.boton,
              },
            },
 {
              type: 'text',
              name: 'adress',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 50,
                message: 'La Direccion debe tener un limite de 50 caracteres',
              },
              },
              textInputProps: {
                label: 'Direccion',

activeOutlineColor: "#87CEFA",
            style: styles.boton,
              },
            },
            {
              type: 'password',
              name: 'contrasena',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 30,
                message: 'La contrasena debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Contrasena',

activeOutlineColor: "#87CEFA",
            style: styles.boton,
              },
            },

            {
              type: 'text',
              name: 'documento',
              rules: {
                required: {
                  value: 1,
                  message: 'Este campo es requerido',
                },
maxLength: {
                value: 30,
                message: 'El numero de documento debe tener un limite de 30 caracteres',
              },
              },
              textInputProps: {
                label: 'Numero de cedula',

activeOutlineColor: "#87CEFA",
            style: styles.boton,
              },
            }
          ]}
        />










  

<Button buttonColor="#87CEFA" mode='contained'  style={styles.modal}    onPress={handleSubmit((data: any) => {
setPago(1);

 

crearusuario(data.nombre,data.apellido,data.email,data.contrasena,data.documento,data.nombre2,data.telefono,data.adress);

          })}   contentStyle={styles.buttoncontent} >
    Crear usuario  
  </Button>


<ActivityIndicator animating={pago}  />









 </ScrollView>

  );
}



const styles = StyleSheet.create({
boton: {

marginHorizontal: "2%",

 

  },

modal: {

margin: 50,

height: 50,








  },

outlined: {

margin: 50,

height: 50,








  },
buttoncontent: {

width: "100%",
height: "100%",

  },


//...
});

