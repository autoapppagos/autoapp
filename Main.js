import * as React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { TextInput,Button,Menu  } from 'react-native-paper';
import EncryptedStorage from 'react-native-encrypted-storage';
import { Image,ScrollView  } from 'react-native';
import axios from "axios";


import Boton from './Boton.js';
import Botonmenu from './Botonmenu.js';
import Screen from './Screen.js';


import Notifications from './Notifications.js';
export default function Main({ navigation }) {

 const [nombre, setNombre] = React.useState("");
const [menu, setMenu] = React.useState(0);
const [carros, setCarros] = React.useState([]);
const [actual, setActual] = React.useState("");

const [actualmenu, setActualmenu] = React.useState("");

const [botones, setBotones] = React.useState([{label: "Informacion",documento: require("./carcontained.png"),icon: require("./carcontained.png"),documentocontained: require("./car.png")},{label: "Multas",documento: require("./cash.png"),icon: require("./cash.png"),documentocontained: require("./cashcontained.png")},{label: "Documentos",documento: require("./folder.png"),icon: require("./folder.png"),documentocontained: require("./foldercontained.png")},{label: "Retiros",documento: require("./multa.png"),icon: require("./multa.png"),documentocontained: require("./multacontained.png")}]);





const baseURL = "http://192.168.50.16:5200";







async function borrar() {
    try {
        await EncryptedStorage.clear();
navigation.navigate('Login')
        // Congrats! You've just cleared the device storage!
    } catch (error) {
        // There was an error on the native side
    }
}



async function cargar() {
    try {   
        const session = await EncryptedStorage.getItem("nombre");
setNombre(session);
const email = await EncryptedStorage.getItem("email");


axios.get(`${baseURL}/carros/${email}`).then((response) => {


if(actual===""){

response.data.map((carro) => { 

carro.color="white";



});

response.data[0].color="#87CEFA" ;


}











console.log(response.data);

setCarros(response.data);
setActual(response.data[0].placa);




console.log(carros);
      
    }).catch(error => {
      console.log(error);
    });


    
      
    } catch (error) {
        // There was an error on the native side
    }
}

React.useEffect(() => {


if(actualmenu!==""){

const botones2 = botones.map(a => Object.assign({}, a));





botones2.map((boton) => { 

boton.color="white";
boton.documento=boton.icon;

if(boton.label===actualmenu){



boton.color="#87CEFA"  ;
boton.documento=boton.documentocontained;


}



});
console.log(botones2);


setBotones(botones2);






}





     
	

    return () => {
      
    };
  }, [actualmenu]);


React.useEffect(() => {

cargar();




     
	

    return () => {
      
    };
  }, []);

React.useEffect(() => {


if(actual!==""){

const carros2 = carros.map(a => Object.assign({}, a));





carros2.map((carro) => { 

carro.color="white";

if(carro.placa===actual){



carro.color="#87CEFA";


}



});


setCarros(carros2);






}





     
	

    return () => {
      
    };
  }, [actual]);


React.useEffect(() => {





navigation.setOptions({ title: `Hola ${nombre}` ,headerRight: () => (

 <Notifications navigation={navigation} />



 
            
          ),headerLeft: () => (

 <ScrollView horizontal >

 <Button
			  
              
            />

 </ScrollView>
            
          )});
     
	

    return () => {
      
    };
  }, [nombre,menu]);


React.useEffect(() => {





setActualmenu("Multas");
     
	

    return () => {
      
    };
  }, []);


 


  return (

 <View                      style={styles.main}     >                

<Button textColor="#87CEFA" mode="outlined" onPress={() => navigation.navigate('Placa')} style={styles.boton} >
    + Registrar vehiculo 
  </Button>
 <ScrollView horizontal   style={styles.carros}>

  {
                carros.map((carro) => { return  ( 

 <Boton placa={carro.placa} setActual={setActual} color={carro.color} actual={actual} key={carro.placa} />

)





})


            }

 </ScrollView>


<Screen   actual={actual} actualmenu={actualmenu} navigation={navigation}  />
























<View  style={styles.content}>


{
                botones.map((carro) => { return  ( 

 <Botonmenu label={carro.label} setActualmenu={setActualmenu} actualmenu={actualmenu} documento={carro.documento} key={carro.label} color={carro.color}   />

)

})


            }

    






    




    






    


</View>





</View>
  );
}

const styles = StyleSheet.create({
   button: {
   

marginHorizontal: 0,
width: "24%",
height: 100,

  },
 modal: {
    backgroundColor: "white",
margin: 20,
marginHorizontal: 10,
width: "95%",
height: 150,

  },
buttoncontent: {

width: "100%",
height: "100%",
flexDirection: 'column'

  },
surface: {
margin: 20,
    height: 80,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
 backgroundColor: "white",

    

    
  },

carros: {

    height: 110,
    width: "100%",
    


    

    
  },

screen: {

    height: 80,
    width: "100%",
    
 backgroundColor: "white",

    

    
  },

text: {
    fontSize: 20,
    fontWeight: 'bold',
marginHorizontal: 30,
  },

icon: {

marginRigth: 30,

height: 40,
    width: 40,




  },

boton: {

margin: 30,

height: 40,
    width: "80%",
backgroundColor: "white",




  },
content: {

backgroundColor: "white",
    
    flexDirection: "row",
  },

main: {


backgroundColor: "white",


height: "100%",

  },
//...
});